import L from './leaflet/leaflet.js';
import moment from 'moment';
import './leaflet/leaflet.rotatedMarker.js';

import appEvents from 'app/core/app_events';
import {MetricsPanelCtrl} from 'app/plugins/sdk';

import './leaflet/leaflet.css!';
import './partials/module.css!';

const panelDefaults = {
  maxDataPoints: 500,
  autoZoom: true,
  scrollWheelZoom: false,
  defaultLayer: 'OpenStreetMap',
  lineColor: 'red',
  pointColor: 'royalblue',
}

function log(msg) {
  // uncomment for debugging
  //console.log(msg);
}

export class ShipMapCtrl extends MetricsPanelCtrl {
  constructor($scope, $injector) {
    super($scope, $injector);

    log("constructor");

    _.defaults(this.panel, panelDefaults);

    // Save layers globally in order to use them in options
    this.layers = {
      'TransportMap': L.tileLayer('https://tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=e4801061a67240d58d854dad92b8eb72', {
        attribution: '',
        maxZoom: 19
      }),
      'TransportMap-Dark': L.tileLayer('https://tile.thunderforest.com/transport-dark/{z}/{x}/{y}.png?apikey=e4801061a67240d58d854dad92b8eb72', {
        attribution: '',
        maxZoom: 19
      }),
      'OpenStreetMap': L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '',
        maxZoom: 19
      }),
      'OpenTopoMap': L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
        attribution: '',
        maxZoom: 17
      }),
      'Satellite': L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        attribution: '',
        // This map doesn't have labels so we force a label-only layer on top of it
        forcedOverlay: L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-labels/{z}/{x}/{y}.png', {
          //attribution: 'Labels by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
          subdomains: 'abcd',
          maxZoom: 20,
        })
      })
    };

    this.timeSrv = $injector.get('timeSrv');
    this.coords = [];
    this.leafMap = null;
    this.polyline = null;
    this.hoverMarker = null;
    this.hoverTarget = null;
    this.setSizePromise = null;

    // Panel events
    this.events.on('panel-initialized', this.onInitialized.bind(this));
    this.events.on('view-mode-changed', this.onViewModeChanged.bind(this));
    this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
    this.events.on('panel-teardown', this.onPanelTeardown.bind(this));
    this.events.on('panel-size-changed', this.onPanelSizeChanged.bind(this));
    this.events.on('data-received', this.onDataReceived.bind(this));
    this.events.on('data-snapshot-load', this.onDataSnapshotLoad.bind(this));

    // Global events
    appEvents.on('graph-hover', this.onPanelHover.bind(this));
    appEvents.on('graph-hover-clear', this.onPanelClear.bind(this));
  }

  onInitialized(){
    log("onInitialized");
    this.render();
  }

  onInitEditMode() {
    log("onInitEditMode");
    this.addEditorTab('Options', 'public/plugins/grafana-shipmap-panel/partials/options.html', 2);
  }

  onPanelTeardown() {
    log("onPanelTeardown");
    this.$timeout.cancel(this.setSizePromise);
  }

  onPanelHover(evt) {
    log("onPanelHover");
    if (this.coords.length === 0) {
      return;
    }

    // check if we are already showing the correct hoverMarker
    let target = Math.floor(evt.pos.x);
    if (this.hoverTarget && this.hoverTarget === target) {
      return;
    }

    // check for initial show of the marker
    if (this.hoverTarget == null){
      this.hoverMarker.bindTooltip("", {'permanent': true, 'offset': L.point(0,20), 'className': 'shipmap-tooltip'} ).addTo(this.leafMap);
    }

    this.hoverTarget = target;

    // Find the currently selected time and move the hoverMarker to it
    // Note that an exact match isn't always going to work due to rounding so
    // we clean that up later (still more efficient)
    let min = 0;
    let max = this.coords.length - 1;
    let idx = null;
    let exact = false;
    while (min <= max) {
      idx = Math.floor((max + min) / 2);
      if (this.coords[idx].timestamp === this.hoverTarget) {
        exact = true;
        break;
      }
      else if (this.coords[idx].timestamp < this.hoverTarget) {
        min = idx + 1;
      }
      else {
        max = idx - 1;
      }
    }

    // Correct the case where we are +1 index off
    if (!exact && idx > 0 && this.coords[idx].timestamp > this.hoverTarget) {
      idx--;
    }
    this.hoverMarker.setLatLng(this.coords[idx].position);

    // Construct tooltip text
    let ttt = "";
    ttt += "<span style='font-weight: bold'>" + this.coords[idx].name + "</span><br/>";
    ttt += "<span>" + moment.unix(this.coords[idx].timestamp/1000.0).format("DD-MM-YYYY T HH:mm") + " UTC</span><br/>";
    ttt += "<span>" + parseFloat(this.coords[idx].latitude).toFixed(2) + "N "+ parseFloat(this.coords[idx].longitude).toFixed(2) + "E</span><br/>";
    // loop over keys in object
    for (var prop in this.coords[idx]) {
      if (prop !== "position" && prop !== "timestamp" && prop !== "name" && prop !== "latitude" && prop !== "longitude" ) {
        ttt += "<span>" + prop + ": " + parseFloat(this.coords[idx][prop]).toFixed(2) + "</span><br/>";
      }
    }


    this.hoverMarker.setTooltipContent(ttt);

    var angle;

    console.log(this.coords);

    if ('heading' in this.coords[idx]) {
      // HEADING PROVIDED IN DATA QUERY
      let H = this.coords[idx].heading;
      angle = H * 180.0 / Math.PI + 180.0;
    }
    else {
      // HEADING BASED DERIVED FROM TRACK
      // Set marker roatation (i.e. ship heading)
      // TODO: Get heading as an actual Data query parameter,
      // For now: Derive heading from ship path...
      let p1 = this.coords[idx].position;
      let p2 = this.coords[idx+1].position;
      // handle last coord point case... (pick previous instead)
      if (idx === this.coords.length-1) p2 = this.coords[idx-2].position;

      let xy1 = this.leafMap.project( L.latLng(p1) );
      let xy2 = this.leafMap.project( L.latLng(p2) );
      let dx = xy1.x - xy2.x;
      let dy = xy1.y - xy2.y;
      angle = Math.atan2(dx,-dy) * 180.0 / Math.PI;
    }

    this.hoverMarker.options.rotationAngle = angle;
  }

  onPanelClear(evt) {
    log("onPanelClear");
    // clear the highlighted circle
    this.hoverTarget = null;
    if (this.hoverMarker) {
      this.hoverMarker.removeFrom(this.leafMap);
    }
  }

  onViewModeChanged(){
    log("onViewModeChanged");
    // KLUDGE: When the view mode is changed, panel resize events are not
    //         emitted even if the panel was resized. Work around this by telling
    //         the panel it's been resized whenever the view mode changes.
    this.onPanelSizeChanged();
  }

  onPanelSizeChanged() {
    log("onPanelSizeChanged");
    // KLUDGE: This event is fired too soon - we need to delay doing the actual
    //         size invalidation until after the panel has actually been resized.
    this.$timeout.cancel(this.setSizePromise);
    let map = this.leafMap;
    this.setSizePromise = this.$timeout(function(){
      if (map) {
        log("Invalidating map size");
        map.invalidateSize(true);
      }}, 500
    );
  }

  applyScrollZoom() {
    let enabled = this.leafMap.scrollWheelZoom.enabled();
    if (enabled != this.panel.scrollWheelZoom){
      if (enabled){
        this.leafMap.scrollWheelZoom.disable();
      }
      else{
        this.leafMap.scrollWheelZoom.enable();
      }
    }
  }

  applyDefaultLayer() {
    let hadMap = Boolean(this.leafMap);
    this.setupMap();
    // Only need to re-add layers if the map previously existed
    if (hadMap){
      this.leafMap.eachLayer((layer) => {
        layer.removeFrom(this.leafMap);
      });
      this.layers[this.panel.defaultLayer].addTo(this.leafMap);
    }
    this.addDataToMap();
  }

  setupMap() {
    log("setupMap");
    // Create the map or get it back in a clean state if it already exists
    if (this.leafMap) {
      if (this.polyline) {
        this.polyline.removeFrom(this.leafMap);
      }
      this.onPanelClear();
      return;
    }

    // Create the map
    this.leafMap = L.map('trackmap-' + this.panel.id, {
      scrollWheelZoom: this.panel.scrollWheelZoom,
      zoomSnap: 0.5,
      zoomDelta: 1,
    });

    // Add layers to the control widget
    L.control.layers(this.layers).addTo(this.leafMap);

    // Add default layer to map
    this.layers[this.panel.defaultLayer].addTo(this.leafMap);

    // Old Hover marker
    /*this.hoverMarkerOld = L.circleMarker(L.latLng(0, 0), {
      color: 'white',
      fillColor: this.panel.pointColor,
      fillOpacity: 1,
      weight: 2,
      radius: 7
    });*/

    var myIcon = L.icon({
      iconUrl: 'public/plugins/grafana-shipmap-panel/img/arrow.png',
      iconSize: [20, 20],
      iconAnchor: [10, 10],
      /*popupAnchor: [0, 0],
      shadowUrl: 'public/plugins/grafana-shipmap-panel/img/arrow.png',
      shadowSize: [20, 20],
      shadowAnchor: [15, 15]*/
    });
    this.hoverMarker = L.marker(L.latLng(0,0), {icon: myIcon, rotationAngle: 45.0})

    // Start/Stop marker
    this.startMarker = L.circleMarker(L.latLng(0, 0), {
      color: 'white',
      fillColor: 'red',
      fillOpacity: 1,
      weight: 2,
      radius: 9
    });
    // Start/Stop marker
    this.stopMarker = L.circleMarker(L.latLng(0, 0), {
      color: 'white',
      fillColor: 'green',
      fillOpacity: 1,
      weight: 2,
      radius: 9
    });

    // Events
    this.leafMap.on('baselayerchange', this.mapBaseLayerChange.bind(this));
    this.leafMap.on('boxzoomend', this.mapZoomToBox.bind(this));
  }

  mapBaseLayerChange(e) {
    // If a tileLayer has a 'forcedOverlay' attribute, always enable/disable it
    // along with the layer
    if (this.leafMap.forcedOverlay) {
      this.leafMap.forcedOverlay.removeFrom(this.leafMap);
      this.leafMap.forcedOverlay = null;
    }
    let overlay = e.layer.options.forcedOverlay;
    if (overlay) {
      overlay.addTo(this.leafMap);
      overlay.setZIndex(e.layer.options.zIndex + 1);
      this.leafMap.forcedOverlay = overlay;
    }
  }

  mapZoomToBox(e) {
    log("mapZoomToBox");
    // Find time bounds of selected coordinates
    const bounds = this.coords.reduce(
      function(t, c) {
        if (e.boxZoomBounds.contains(c.position)) {
          t.from = Math.min(t.from, c.timestamp);
          t.to = Math.max(t.to, c.timestamp);
        }
        return t;
      },
      {from: Infinity, to: -Infinity}
    );

    // Set the global time range
    if (isFinite(bounds.from) && isFinite(bounds.to)) {
      // KLUDGE: Create moment objects here to avoid a TypeError that
      //         occurs when Grafana processes normal numbers
      this.timeSrv.setTime({
        from: moment.utc(bounds.from),
        to: moment.utc(bounds.to)
      });
    }
  }

  // Add the circles and polyline to the map
  addDataToMap() {
    log("addDataToMap");

    // the polyline
    this.polyline = L.polyline(
      this.coords.map(x => x.position, this), {
        color: this.panel.lineColor,
        weight: 3,
      }
    ).addTo(this.leafMap);

    // the start stop markers
    this.startMarker.setLatLng(this.coords[0].position);
    this.stopMarker.setLatLng(this.coords[ this.coords.length-1 ].position);
    this.startMarker.addTo(this.leafMap);
    this.stopMarker.addTo(this.leafMap);

    this.zoomToFit();
  }

  zoomToFit(){
    log("zoomToFit");
    if (this.panel.autoZoom && this.polyline){
      this.leafMap.fitBounds(this.polyline.getBounds());
    }
    this.render();
  }

  refreshColors() {
    log("refreshColors");
    if (this.polyline) {
      this.polyline.setStyle({
        color: this.panel.lineColor
      });
    }
    if (this.hoverMarker){
      this.hoverMarker.setStyle({
        fillColor: this.panel.pointColor,
      });
    }
    this.render();
  }

  onDataReceived(data) {
    log("onDataReceived");
    this.setupMap();

    // find latitude and longitude table columns
    let params = {};
    for (let i=0; i<data.length; i++) {
      //if( data[i].target.endsWith(".latitude") ) lats = data[i].datapoints;
      //else if( data[i].target.endsWith(".longitude") ) lons = data[i].datapoints;
      //else if( data[i].target.endsWith(".name") ) names = data[i].datapoints;
      //else {
        let splt = data[i].target.split("."); // skippping any db.keyname form from query
        let key = splt[splt.length-1];
        console.log("inserting "+key);
        params[key] = data[i].datapoints;
      //}
    }

    // Asumption is that there are an equal number of properly matched timestamps
    // TODO: proper joining by timestamp?
    this.coords.length = 0;

    // Collect values here for plotting ( trying to handle some exceptions / bad data )
    for (let i = 0; i < params.latitude.length; i++) {
      if (params.latitude[i][0] == null || params.longitude[i][0] == null) {
        continue;
      }

      // some standard values required
      let datum = {
        position: L.latLng(params.latitude[i][0], params.longitude[i][0]),
        timestamp: params.latitude[i][1],
      };

      // other parameters from query for display
      for (var key in params) {
        if ( isNaN( params[key][i][0] ) ) datum[key] = params[key][i][0];
        else datum[key] = params[key][i][0];
      };

      this.coords.push(datum);
    }
    this.addDataToMap();
  }

  onDataSnapshotLoad(snapshotData) {
    log("onSnapshotLoad");
    this.onDataReceived(snapshotData);
  }
}

ShipMapCtrl.templateUrl = 'partials/module.html';
