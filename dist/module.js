System.register(["./shipmap_ctrl"], function (_export, _context) {
  "use strict";

  var ShipMapCtrl;
  return {
    setters: [function (_shipmap_ctrl) {
      ShipMapCtrl = _shipmap_ctrl.ShipMapCtrl;
    }],
    execute: function () {
      _export("PanelCtrl", ShipMapCtrl);
    }
  };
});
//# sourceMappingURL=module.js.map
