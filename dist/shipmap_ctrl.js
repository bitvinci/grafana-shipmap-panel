System.register(["./leaflet/leaflet.js", "moment", "./leaflet/leaflet.rotatedMarker.js", "app/core/app_events", "app/plugins/sdk", "./leaflet/leaflet.css!", "./partials/module.css!"], function (_export, _context) {
  "use strict";

  var L, moment, appEvents, MetricsPanelCtrl, panelDefaults, ShipMapCtrl;

  function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

  function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

  function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

  function log(msg) {// uncomment for debugging
    //console.log(msg);
  }

  return {
    setters: [function (_leafletLeafletJs) {
      L = _leafletLeafletJs.default;
    }, function (_moment) {
      moment = _moment.default;
    }, function (_leafletLeafletRotatedMarkerJs) {}, function (_appCoreApp_events) {
      appEvents = _appCoreApp_events.default;
    }, function (_appPluginsSdk) {
      MetricsPanelCtrl = _appPluginsSdk.MetricsPanelCtrl;
    }, function (_leafletLeafletCss) {}, function (_partialsModuleCss) {}],
    execute: function () {
      panelDefaults = {
        maxDataPoints: 500,
        autoZoom: true,
        scrollWheelZoom: false,
        defaultLayer: 'OpenStreetMap',
        lineColor: 'red',
        pointColor: 'royalblue'
      };

      _export("ShipMapCtrl", ShipMapCtrl =
      /*#__PURE__*/
      function (_MetricsPanelCtrl) {
        _inherits(ShipMapCtrl, _MetricsPanelCtrl);

        function ShipMapCtrl($scope, $injector) {
          var _this;

          _classCallCheck(this, ShipMapCtrl);

          _this = _possibleConstructorReturn(this, _getPrototypeOf(ShipMapCtrl).call(this, $scope, $injector));
          log("constructor");

          _.defaults(_this.panel, panelDefaults); // Save layers globally in order to use them in options


          _this.layers = {
            'TransportMap': L.tileLayer('https://tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=e4801061a67240d58d854dad92b8eb72', {
              attribution: '',
              maxZoom: 19
            }),
            'TransportMap-Dark': L.tileLayer('https://tile.thunderforest.com/transport-dark/{z}/{x}/{y}.png?apikey=e4801061a67240d58d854dad92b8eb72', {
              attribution: '',
              maxZoom: 19
            }),
            'OpenStreetMap': L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              attribution: '',
              maxZoom: 19
            }),
            'OpenTopoMap': L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
              attribution: '',
              maxZoom: 17
            }),
            'Satellite': L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
              attribution: '',
              // This map doesn't have labels so we force a label-only layer on top of it
              forcedOverlay: L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-labels/{z}/{x}/{y}.png', {
                //attribution: 'Labels by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: 'abcd',
                maxZoom: 20
              })
            })
          };
          _this.timeSrv = $injector.get('timeSrv');
          _this.coords = [];
          _this.leafMap = null;
          _this.polyline = null;
          _this.hoverMarker = null;
          _this.hoverTarget = null;
          _this.setSizePromise = null; // Panel events

          _this.events.on('panel-initialized', _this.onInitialized.bind(_assertThisInitialized(_this)));

          _this.events.on('view-mode-changed', _this.onViewModeChanged.bind(_assertThisInitialized(_this)));

          _this.events.on('init-edit-mode', _this.onInitEditMode.bind(_assertThisInitialized(_this)));

          _this.events.on('panel-teardown', _this.onPanelTeardown.bind(_assertThisInitialized(_this)));

          _this.events.on('panel-size-changed', _this.onPanelSizeChanged.bind(_assertThisInitialized(_this)));

          _this.events.on('data-received', _this.onDataReceived.bind(_assertThisInitialized(_this)));

          _this.events.on('data-snapshot-load', _this.onDataSnapshotLoad.bind(_assertThisInitialized(_this))); // Global events


          appEvents.on('graph-hover', _this.onPanelHover.bind(_assertThisInitialized(_this)));
          appEvents.on('graph-hover-clear', _this.onPanelClear.bind(_assertThisInitialized(_this)));
          return _this;
        }

        _createClass(ShipMapCtrl, [{
          key: "onInitialized",
          value: function onInitialized() {
            log("onInitialized");
            this.render();
          }
        }, {
          key: "onInitEditMode",
          value: function onInitEditMode() {
            log("onInitEditMode");
            this.addEditorTab('Options', 'public/plugins/grafana-shipmap-panel/partials/options.html', 2);
          }
        }, {
          key: "onPanelTeardown",
          value: function onPanelTeardown() {
            log("onPanelTeardown");
            this.$timeout.cancel(this.setSizePromise);
          }
        }, {
          key: "onPanelHover",
          value: function onPanelHover(evt) {
            log("onPanelHover");

            if (this.coords.length === 0) {
              return;
            } // check if we are already showing the correct hoverMarker


            var target = Math.floor(evt.pos.x);

            if (this.hoverTarget && this.hoverTarget === target) {
              return;
            } // check for initial show of the marker


            if (this.hoverTarget == null) {
              this.hoverMarker.bindTooltip("", {
                'permanent': true,
                'offset': L.point(0, 20),
                'className': 'shipmap-tooltip'
              }).addTo(this.leafMap);
            }

            this.hoverTarget = target; // Find the currently selected time and move the hoverMarker to it
            // Note that an exact match isn't always going to work due to rounding so
            // we clean that up later (still more efficient)

            var min = 0;
            var max = this.coords.length - 1;
            var idx = null;
            var exact = false;

            while (min <= max) {
              idx = Math.floor((max + min) / 2);

              if (this.coords[idx].timestamp === this.hoverTarget) {
                exact = true;
                break;
              } else if (this.coords[idx].timestamp < this.hoverTarget) {
                min = idx + 1;
              } else {
                max = idx - 1;
              }
            } // Correct the case where we are +1 index off


            if (!exact && idx > 0 && this.coords[idx].timestamp > this.hoverTarget) {
              idx--;
            }

            this.hoverMarker.setLatLng(this.coords[idx].position); // Construct tooltip text

            var ttt = "";
            ttt += "<span style='font-weight: bold'>" + this.coords[idx].name + "</span><br/>";
            ttt += "<span>" + moment.unix(this.coords[idx].timestamp / 1000.0).format("DD-MM-YYYY T HH:mm") + " UTC</span><br/>";
            ttt += "<span>" + parseFloat(this.coords[idx].latitude).toFixed(2) + "N " + parseFloat(this.coords[idx].longitude).toFixed(2) + "E</span><br/>"; // loop over keys in object

            for (var prop in this.coords[idx]) {
              if (prop !== "position" && prop !== "timestamp" && prop !== "name" && prop !== "latitude" && prop !== "longitude") {
                ttt += "<span>" + prop + ": " + parseFloat(this.coords[idx][prop]).toFixed(2) + "</span><br/>";
              }
            }

            this.hoverMarker.setTooltipContent(ttt);
            var angle;
            console.log(this.coords);

            if ('heading' in this.coords[idx]) {
              // HEADING PROVIDED IN DATA QUERY
              var H = this.coords[idx].heading;
              angle = H * 180.0 / Math.PI + 180.0;
            } else {
              // HEADING BASED DERIVED FROM TRACK
              // Set marker roatation (i.e. ship heading)
              // TODO: Get heading as an actual Data query parameter,
              // For now: Derive heading from ship path...
              var p1 = this.coords[idx].position;
              var p2 = this.coords[idx + 1].position; // handle last coord point case... (pick previous instead)

              if (idx === this.coords.length - 1) p2 = this.coords[idx - 2].position;
              var xy1 = this.leafMap.project(L.latLng(p1));
              var xy2 = this.leafMap.project(L.latLng(p2));
              var dx = xy1.x - xy2.x;
              var dy = xy1.y - xy2.y;
              angle = Math.atan2(dx, -dy) * 180.0 / Math.PI;
            }

            this.hoverMarker.options.rotationAngle = angle;
          }
        }, {
          key: "onPanelClear",
          value: function onPanelClear(evt) {
            log("onPanelClear"); // clear the highlighted circle

            this.hoverTarget = null;

            if (this.hoverMarker) {
              this.hoverMarker.removeFrom(this.leafMap);
            }
          }
        }, {
          key: "onViewModeChanged",
          value: function onViewModeChanged() {
            log("onViewModeChanged"); // KLUDGE: When the view mode is changed, panel resize events are not
            //         emitted even if the panel was resized. Work around this by telling
            //         the panel it's been resized whenever the view mode changes.

            this.onPanelSizeChanged();
          }
        }, {
          key: "onPanelSizeChanged",
          value: function onPanelSizeChanged() {
            log("onPanelSizeChanged"); // KLUDGE: This event is fired too soon - we need to delay doing the actual
            //         size invalidation until after the panel has actually been resized.

            this.$timeout.cancel(this.setSizePromise);
            var map = this.leafMap;
            this.setSizePromise = this.$timeout(function () {
              if (map) {
                log("Invalidating map size");
                map.invalidateSize(true);
              }
            }, 500);
          }
        }, {
          key: "applyScrollZoom",
          value: function applyScrollZoom() {
            var enabled = this.leafMap.scrollWheelZoom.enabled();

            if (enabled != this.panel.scrollWheelZoom) {
              if (enabled) {
                this.leafMap.scrollWheelZoom.disable();
              } else {
                this.leafMap.scrollWheelZoom.enable();
              }
            }
          }
        }, {
          key: "applyDefaultLayer",
          value: function applyDefaultLayer() {
            var _this2 = this;

            var hadMap = Boolean(this.leafMap);
            this.setupMap(); // Only need to re-add layers if the map previously existed

            if (hadMap) {
              this.leafMap.eachLayer(function (layer) {
                layer.removeFrom(_this2.leafMap);
              });
              this.layers[this.panel.defaultLayer].addTo(this.leafMap);
            }

            this.addDataToMap();
          }
        }, {
          key: "setupMap",
          value: function setupMap() {
            log("setupMap"); // Create the map or get it back in a clean state if it already exists

            if (this.leafMap) {
              if (this.polyline) {
                this.polyline.removeFrom(this.leafMap);
              }

              this.onPanelClear();
              return;
            } // Create the map


            this.leafMap = L.map('trackmap-' + this.panel.id, {
              scrollWheelZoom: this.panel.scrollWheelZoom,
              zoomSnap: 0.5,
              zoomDelta: 1
            }); // Add layers to the control widget

            L.control.layers(this.layers).addTo(this.leafMap); // Add default layer to map

            this.layers[this.panel.defaultLayer].addTo(this.leafMap); // Old Hover marker

            /*this.hoverMarkerOld = L.circleMarker(L.latLng(0, 0), {
              color: 'white',
              fillColor: this.panel.pointColor,
              fillOpacity: 1,
              weight: 2,
              radius: 7
            });*/

            var myIcon = L.icon({
              iconUrl: 'public/plugins/grafana-shipmap-panel/img/arrow.png',
              iconSize: [20, 20],
              iconAnchor: [10, 10]
              /*popupAnchor: [0, 0],
              shadowUrl: 'public/plugins/grafana-shipmap-panel/img/arrow.png',
              shadowSize: [20, 20],
              shadowAnchor: [15, 15]*/

            });
            this.hoverMarker = L.marker(L.latLng(0, 0), {
              icon: myIcon,
              rotationAngle: 45.0
            }); // Start/Stop marker

            this.startMarker = L.circleMarker(L.latLng(0, 0), {
              color: 'white',
              fillColor: 'red',
              fillOpacity: 1,
              weight: 2,
              radius: 9
            }); // Start/Stop marker

            this.stopMarker = L.circleMarker(L.latLng(0, 0), {
              color: 'white',
              fillColor: 'green',
              fillOpacity: 1,
              weight: 2,
              radius: 9
            }); // Events

            this.leafMap.on('baselayerchange', this.mapBaseLayerChange.bind(this));
            this.leafMap.on('boxzoomend', this.mapZoomToBox.bind(this));
          }
        }, {
          key: "mapBaseLayerChange",
          value: function mapBaseLayerChange(e) {
            // If a tileLayer has a 'forcedOverlay' attribute, always enable/disable it
            // along with the layer
            if (this.leafMap.forcedOverlay) {
              this.leafMap.forcedOverlay.removeFrom(this.leafMap);
              this.leafMap.forcedOverlay = null;
            }

            var overlay = e.layer.options.forcedOverlay;

            if (overlay) {
              overlay.addTo(this.leafMap);
              overlay.setZIndex(e.layer.options.zIndex + 1);
              this.leafMap.forcedOverlay = overlay;
            }
          }
        }, {
          key: "mapZoomToBox",
          value: function mapZoomToBox(e) {
            log("mapZoomToBox"); // Find time bounds of selected coordinates

            var bounds = this.coords.reduce(function (t, c) {
              if (e.boxZoomBounds.contains(c.position)) {
                t.from = Math.min(t.from, c.timestamp);
                t.to = Math.max(t.to, c.timestamp);
              }

              return t;
            }, {
              from: Infinity,
              to: -Infinity
            }); // Set the global time range

            if (isFinite(bounds.from) && isFinite(bounds.to)) {
              // KLUDGE: Create moment objects here to avoid a TypeError that
              //         occurs when Grafana processes normal numbers
              this.timeSrv.setTime({
                from: moment.utc(bounds.from),
                to: moment.utc(bounds.to)
              });
            }
          } // Add the circles and polyline to the map

        }, {
          key: "addDataToMap",
          value: function addDataToMap() {
            log("addDataToMap"); // the polyline

            this.polyline = L.polyline(this.coords.map(function (x) {
              return x.position;
            }, this), {
              color: this.panel.lineColor,
              weight: 3
            }).addTo(this.leafMap); // the start stop markers

            this.startMarker.setLatLng(this.coords[0].position);
            this.stopMarker.setLatLng(this.coords[this.coords.length - 1].position);
            this.startMarker.addTo(this.leafMap);
            this.stopMarker.addTo(this.leafMap);
            this.zoomToFit();
          }
        }, {
          key: "zoomToFit",
          value: function zoomToFit() {
            log("zoomToFit");

            if (this.panel.autoZoom && this.polyline) {
              this.leafMap.fitBounds(this.polyline.getBounds());
            }

            this.render();
          }
        }, {
          key: "refreshColors",
          value: function refreshColors() {
            log("refreshColors");

            if (this.polyline) {
              this.polyline.setStyle({
                color: this.panel.lineColor
              });
            }

            if (this.hoverMarker) {
              this.hoverMarker.setStyle({
                fillColor: this.panel.pointColor
              });
            }

            this.render();
          }
        }, {
          key: "onDataReceived",
          value: function onDataReceived(data) {
            log("onDataReceived");
            this.setupMap(); // find latitude and longitude table columns

            var params = {};

            for (var i = 0; i < data.length; i++) {
              //if( data[i].target.endsWith(".latitude") ) lats = data[i].datapoints;
              //else if( data[i].target.endsWith(".longitude") ) lons = data[i].datapoints;
              //else if( data[i].target.endsWith(".name") ) names = data[i].datapoints;
              //else {
              var splt = data[i].target.split("."); // skippping any db.keyname form from query

              var _key = splt[splt.length - 1];
              console.log("inserting " + _key);
              params[_key] = data[i].datapoints; //}
            } // Asumption is that there are an equal number of properly matched timestamps
            // TODO: proper joining by timestamp?


            this.coords.length = 0; // Collect values here for plotting ( trying to handle some exceptions / bad data )

            for (var _i = 0; _i < params.latitude.length; _i++) {
              if (params.latitude[_i][0] == null || params.longitude[_i][0] == null) {
                continue;
              } // some standard values required


              var datum = {
                position: L.latLng(params.latitude[_i][0], params.longitude[_i][0]),
                timestamp: params.latitude[_i][1]
              }; // other parameters from query for display

              for (var key in params) {
                if (isNaN(params[key][_i][0])) datum[key] = params[key][_i][0];else datum[key] = params[key][_i][0];
              }

              ;
              this.coords.push(datum);
            }

            this.addDataToMap();
          }
        }, {
          key: "onDataSnapshotLoad",
          value: function onDataSnapshotLoad(snapshotData) {
            log("onSnapshotLoad");
            this.onDataReceived(snapshotData);
          }
        }]);

        return ShipMapCtrl;
      }(MetricsPanelCtrl));

      ShipMapCtrl.templateUrl = 'partials/module.html';
    }
  };
});
//# sourceMappingURL=shipmap_ctrl.js.map
